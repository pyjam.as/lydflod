FROM python:3.10

RUN apt update && apt install -y gcc vim ffmpeg portaudio19-dev python3-pyaudio mpv mpg321
RUN pip install pyaudio numpy pdbpp
