import pyaudio
import time
import string
import numpy as np
import socket
from threading import Thread
from queue import Queue

port = 1338
samplerate = 44_100
n_frequencies = 20_000

# list of the amplitudes for different frequencies
# frequency is the index (in hz)
# amplitude should be in range [0;1]
frequency_amplitudes = np.zeros(n_frequencies)

# 1 second of audio samples
# this is the main audio which we mutate and loop over to play
# since we only accept frequencies in integer hertz values,
# it can loop with a 1 second period
waveform = np.zeros(samplerate)

# queue between TCP clients state mutation worker
queue = Queue()

def mutation_worker_fun():
    """ This worker just processes incoming frequency state changes sequentially """
    global waveform
    print("Mutation worker go for launch.")
    while True:
        frequency, volume = queue.get()
        waveform = set_amplitude_for_frequency(waveform, frequency, volume / 100)


def set_amplitude_for_frequency(waveform, freq_hz, amplitude):
    """ Adjust `waveform` such that `freq_hz` is present at amplitude `amplitude` """
    frequency = round(freq_hz)
    amplitude_diff = amplitude - frequency_amplitudes[frequency]

    frame_array = np.arange(samplerate)
    scalar = 2 * np.pi * frequency / samplerate
   
    frequency_amplitudes[frequency] = amplitude
    return waveform + amplitude_diff * (np.sin(scalar * frame_array)).astype(np.float32)


def normalize(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))
    
        
def callback(in_data, frame_count, time_info, status):
    """ Callback that pyaudio uses to ask for more samples.
        Just loops around in `waveform`, which is just 1 second
        of loopable audio (may be mutated while mid-playback).
    """
    time_in_frames = int(time_info['input_buffer_adc_time'] * samplerate) 
    start_offset = time_in_frames % samplerate
    start_offset2 = (time_in_frames + frame_count) % samplerate
    
    normed_waveform = (
        normalize(waveform)
        if np.any(waveform > 1) or np.any(waveform < -1)
        else waveform
    )

    slice1 = normed_waveform[start_offset:start_offset + 64]
    slice2 = normed_waveform[0:frame_count-len(slice1)]
    frames = np.concatenate([slice1, slice2]) 
    return (frames.astype('float32').tobytes(), pyaudio.paContinue)


p = pyaudio.PyAudio()
stream = p.open(
    format=pyaudio.paFloat32,
    channels=1,
    rate=samplerate,
    output=True,
    output_device_index=0,
    stream_callback=callback
)
stream.start_stream()


def get_char(client):
    while not (bs := client.recv(1)):
       ...
    assert len(bs) == 1
    return chr(bs[0])


def expect_char(client, c):
    assert get_char(client) == c


def expect_integer(client, endchar):
    int_str = ""
    while (c := get_char(client)) in string.digits:
        int_str += c
    assert c == endchar, f"Expected {endchar}, got {c}"
    return int(int_str)


def threadfun(client):
    while True:
        expect_char(client, "F")
        expect_char(client, "Q")
        expect_char(client, " ")
        frequency = expect_integer(client, endchar=" ")
        assert 0 < frequency < n_frequencies
        volume = expect_integer(client, endchar="\n")
        assert 0 <= volume <= 100

        queue.put((frequency, volume))


if __name__ == "__main__":
    # start the mutation worker
    mutation_worker = Thread(target=mutation_worker_fun)
    mutation_worker.start()

    # open tcp socket
    print(f"Starting TCP listener on port {port}")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("0.0.0.0", port))
    s.listen()

    # start worker threads for each new connection
    while conn := s.accept():
        client, addr = conn
        print(f"New connection from {addr}")
        t = Thread(target=threadfun, args=(client,))
        t.start()
