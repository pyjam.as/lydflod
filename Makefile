dev: docker-build
	docker run \
		--privileged \
		-it \
		--network=host \
		--device /dev/snd \
		-v `pwd`:/pwd \
		-v `pwd`/alsa.conf:/usr/share/alsa/alsa.conf \
		audioflut /bin/bash
		# -v /dev/:/dev/:rw \

docker-build:
	docker build . -t audioflut
