# 🎶 Lydflod

It's kind of like [pixelflut](https://github.com/defnull/pixelflut), but for audio! 🎵

The packet format is like this:

```
FQ <frequency> <volume>\n
```

To play a 440Hz sine wave at 100% volume, send a string like this over TCP:

```
FQ 440 100\n
```

The volume is set per herz-integer frequency, so if you want to stop that same 440Hz note, send:

```
FQ 440 0\n
```

All integer frequencies from 0Hz to 20,000Hz are accepted.

